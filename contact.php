<?php
require_once 'header.php';
?>
	<!-- HERO  -->
	<section id="hero" class="hero-big parallax-section" data-parallax-image="files/uploads/hero-contact.jpg">
		<div class="page-title">
			<h1 class="text-light"><strong>Say hello</strong></h1>
		</div>
	</section>
	<!-- HERO -->

	<!-- PAGEBODY -->
	<section id="page-body">

		<div class="wrapper-small">

			<h3 class="align-center"><strong>Let's talk ...</strong></h3>
			<h6 class="alttitle align-center">We are always happy to make valuable new contacts</h6>

			<form id="contact-form" class="checkform" action="#" target="contact-send.php" method="post" >

				<div class="form-row clearfix">
					<label for="name" class="req">Name *</label>
					<input type="text" name="name" class="name" id="name" value="" placeholder="Name" />
				</div>

				<div class="form-row clearfix">
					<label for="email" class="req">Email *</label>
					<input type="text" name="email" class="email" id="email" value="" placeholder="Email"/>
				</div>

				<div class="form-row clearfix textbox">
					<label for="message" class="req">Message *</label>
					<textarea name="message" class="message" id="message" rows="15" cols="50" placeholder="Message"></textarea>
				</div>

				<div id="form-note">
					<div class="alert alert-error">
						<strong>Error</strong>: Please check your entries!
					</div>
				</div>

				<div class="form-row form-submit">
					<input type="submit" name="submit_form" class="submit" value="Send Message" />
				</div>

				<input type="hidden" name="subject" value="Contact Subject Pond html" />
				<input type="hidden" name="fields" value="name,email,message," />
				<input type="hidden" name="sendto" value="YOUREMAIL" />

			</form>

		</div> <!-- END .wrapper-small -->

		<div class="spacer spacer-big"></div>

		<div class="wrapper-small">

			<h3 class="align-center"><strong>... or visit us</strong></h3>
			<h6 class="alttitle align-center">We would love to meet you</h6>

			<div class="spacer spacer-small"></div>

			<h6 class="align-center"><strong>Avoc Agency</strong></h6>
			<p class="align-center">
				Elisabeth Street 245<br>
				5000 – Sydney<br>
				0111 – 222 33 54<br>
				<a href="#"><strong>contact@yoursite.com</strong></a>
			</p>

		</div> <!-- END .wrapper-small -->

		<div class="spacer spacer-medium"></div>

		<!-- GOOGLE MAP -->
		<div id="map" style="height:400px;"></div>
		<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyCOdKtT5fapH3_OfhV3HFeZjqFs4OfNIew&callback=mapinitialize" type="text/javascript"></script>
		<script type="text/javascript">
            function mapinitialize() {
                var latlng = new google.maps.LatLng(-33.86938,151.204834);
                var myOptions = {
                    zoom: 14,
                    center: latlng,
                    scrollwheel: false,
                    scaleControl: false,
                    disableDefaultUI: false,
                    mapTypeId: google.maps.MapTypeId.ROADMAP,
                    // Google Map Color Styles
                    styles: [{featureType:"landscape",stylers:[{saturation:-100},{lightness:65},{visibility:"on"}]},{featureType:"poi",stylers:[{saturation:-100},{lightness:51},{visibility:"simplified"}]},{featureType:"road.highway",stylers:[{saturation:-100},
                            {visibility:"simplified"}]},{featureType:"road.arterial",stylers:[{saturation:-100},{lightness:30},{visibility:"on"}]},{featureType:"road.local",stylers:[{saturation:-100},{lightness:40},{visibility:"on"}]},{featureType:"transit",stylers:[{saturation:-100},
                            {visibility:"simplified"}]},{featureType:"administrative.province",stylers:[{visibility:"off"}]/**/},{featureType:"administrative.locality",stylers:[{visibility:"off"}]},{featureType:"administrative.neighborhood",stylers:[{visibility:"on"}
                        ]/**/},{featureType:"water",elementType:"labels",stylers:[{visibility:"on"},{lightness:-25},{saturation:-100}]},{featureType:"water",elementType:"geometry",stylers:[{hue:"#ffff00"},{lightness:-25},{saturation:-97}]}]
                };
                var map = new google.maps.Map(document.getElementById("map"),myOptions);

                var image = "files/uploads/map-pin.png";
                var image = new google.maps.MarkerImage("files/uploads/map-pin.png", null, null, null, new google.maps.Size(50,56));
                var marker = new google.maps.Marker({
                    map: map,
                    icon: image,
                    position: map.getCenter()
                });

                var contentString = '<b>Office</b><br>Streetname 13<br>50000 Sydney';
                var infowindow = new google.maps.InfoWindow({
                    content: contentString
                });

                google.maps.event.addListener(marker, 'click', function() {
                    infowindow.open(map,marker);
                });


            }
            mapinitialize();
		</script>
		<!-- GOOGLE MAP -->

	</section>
	<!-- PAGEBODY -->

<?php
require_once 'footer.php';
?>