<!-- FOOTER -->
<footer>
	<div class="footer-inner align-center wrapper">
		<div class="widget">
			<ul class="socialmedia-widget">
				<li class="facebook"><a href="#"></a></li>
				<li class="twitter"><a href="#"></a></li>
				<li class="dribbble"><a href="#"></a></li>
				<li class="behance"><a href="#"></a></li>
				<li class="instagram"><a href="#"></a></li>
			</ul>
		</div>

		<div class="widget copyright">
			<p>Copyright &copy; 2018 by Main Street Digital<br>
				Made with Love by <a href="http://www.nextdm.com">next</a></p>
		</div>

		<a id="backtotop" href="#">Back To Top</a>
	</div>
</footer>
<!-- FOOTER -->


</div> <!-- END #page-content -->
<!-- PAGE CONTENT -->
<!-- SCRIPTS -->
<script type="text/javascript" src="files/js/jquery.pace.js"></script>
<script type="text/javascript" src="files/js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="files/js/jquery.easing.compatibility.js"></script>
<script type="text/javascript" src="files/js/jquery.visible.min.js"></script>
<script type="text/javascript" src="files/js/tweenMax.js"></script>
<script type="text/javascript" src="files/js/jquery.bgvideo.min.js"></script>
<script type="text/javascript" src="files/js/jquery.backgroundparallax.min.js"></script>
<script type="text/javascript" src="files/js/fegfdksgf.js"></script>
<!-- SCRIPTS -->
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-116468879-1"></script>
<script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-116468879-1');
</script>

</body>
</html>