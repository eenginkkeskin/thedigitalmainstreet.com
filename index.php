<?php require_once 'header.php'; ?>
    <div class="test" style="position: fixed;top:20px;left:20px; height: 60px; overflow:hidden; width: 100%; color: red;"></div>

    <!-- HERO  -->
    <section id="hero" class="hero-full text-light videobg-section"
             data-videofile="files/uploads/steven"
             data-videowidth="1280"
             data-videoheight="720"
             data-videoposter="files/uploads/steven.jpg"
             data-videooverlaycolor="#ea4452"
             data-videooverlayopacity="0.7"
             data-sound="false">
        <div class="page-title">
            <h1><strong>Main Street Digital Agency</strong></h1>
        </div>
        <a id="scroll-down" href="#">Scroll Down</a>
    </section>
    <!-- HERO -->

    <!-- PAGEBODY -->
    <section id="page-body">

        <div class="wrapper-small align-center">
            <h3><strong>A PHILADELPHIA WEB DESIGN AGENCY</strong></h3>
            <div class="spacer spacer-mini"></div>
            <h5 class="alttitle">We are dependable digital experts with distinct online solutions. Is your website hard to find? We help your business use digital marketing to get found online.</h5>
            <div class="spacer spacer-mini"></div>
            <p><a href="about.html" class="sr-button">Learn more</a></p>
        </div>

        <div class="spacer spacer-big"></div>

        <div class="horizontal-section" style="background: #f2f2f2;">
            <div class="horizontal-inner wrapper-small">

                <div class="wolf-grid clearfix">

                    <div class="wolf-item wfull wright wolf-text" data-speed="0.7">
                        <div class="wolf-item-inner" style="width: 55%;">
                            <div class="wolf-media">
                                <a href="portfolio-single-3.html" class="wolf-media-link"><img src="files/uploads/thumbnail-stillife.jpg" alt="SEO NAME"></a>
                            </div>

                            <div class="wolf-caption" style="width:55%;">
                                <h5 class="alttitle">Photography</h5>
                                <h4><a href="portfolio-single-3.html" class="wolf-caption-link"><strong>Still Life</strong></a></h4>
                                <p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                            </div>
                        </div>
                    </div>

                    <div class="wolf-item wfull wleft wolf-text" data-speed="0.7">
                        <div class="wolf-item-inner" style="width: 55%;">
                            <div class="wolf-media">
                                <a href="portfolio-single-3.html" class="wolf-media-link"><img src="files/uploads/thumbnail-brucke.jpg" alt="SEO NAME"></a>
                            </div>

                            <div class="wolf-caption" style="width:55%;">
                                <h5 class="alttitle">Branding</h5>
                                <h4><a href="portfolio-single-3.html" class="wolf-caption-link"><strong>Brücke Bier</strong></a></h4>
                                <p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                            </div>
                        </div>
                    </div>

                    <div class="wolf-item wfull wright wolf-text" data-speed="0.7">
                        <div class="wolf-item-inner" style="width: 55%;">
                            <div class="wolf-media">
                                <a href="portfolio-single-3.html" class="wolf-media-link"><img src="files/uploads/thumbnail-sixteenfrans.jpg" alt="SEO NAME"></a>
                            </div>

                            <div class="wolf-caption" style="width:55%;">
                                <h5 class="alttitle">Print</h5>
                                <h4><a href="portfolio-single-3.html" class="wolf-caption-link"><strong>Sixteen & Frans</strong></a></h4>
                                <p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                            </div>
                        </div>
                    </div>

                </div> <!-- END .wolf-grid -->

                <div class="align-center">
                    <h5 class="alttitle">This is just a small selection. You will find further examples of our work in our Portfolio.</h5>
                    <div class="spacer spacer-mini"></div>
                    <p><a href="index.php" class="sr-button">See more</a></p>
                </div>

            </div>
        </div> <!-- END .horizontal-section -->

        <div class="horizontal-section parallax-section text-light" data-parallax-image="files/uploads/agency-parallax.jpg">
            <div class="horizontal-inner wrapper">
                <h3 class="align-center"><strong>How we work</strong></h3>
                <h5 class="alttitle align-center">One of our principles is constant internal communication.</h5>

                <div class="column-section clearfix">
                    <div class="column one-third align-center">
                        <i class="ion ion-ios-pulse ion-4x colored"></i>
                        <h5>Keep pulse going</h5>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                    </div>
                    <div class="column one-third align-center">
                        <i class="ion ion-ios-infinite-outline ion-4x colored"></i>
                        <h5>Pass the limits</h5>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                    </div>
                    <div class="column one-third last-col align-center">
                        <i class="ion ion-ios-lightbulb-outline ion-4x colored"></i>
                        <h5>Great Ideas</h5>
                        <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
                    </div>
                </div>
            </div>
        </div> <!-- END .horizontal-section -->

        <div class="spacer spacer-big"></div>

        <div class="wrapper-small align-center">
            <h3><strong>Want to work with us?</strong></h3>
            <div class="spacer spacer-mini"></div>
            <p><a href="contact.html" class="sr-button">Contact us</a></p>
        </div>

        <div class="spacer spacer-big"></div>

    </section>
    <!-- PAGEBODY -->

<?php require_once 'footer.php'; ?>