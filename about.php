<?php
require_once 'header.php';
?>
<!-- HERO  -->
<section id="hero" class="hero-big parallax-section" data-parallax-image="files/uploads/hero-about.jpg">
	<div class="page-title">
		<h1><strong>About</strong></h1>
	</div>
</section>
<!-- HERO -->
<!-- PAGEBODY -->
<section id="page-body">

	<div class="wrapper">

		<div class="column-section clearfix">
			<div class="column one-third">
				<h3 class="colored"><strong>We are Main Street</strong></h3>
				<h4 class="alttitle">We think that design is not just a tool, it is a way of life.</h4>
			</div>
			<div class="column one-third">
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren.</p>
			</div>
			<div class="column one-third last-col">
				<p>Ut wisi enim ad minim veniam, quis nostrud exerci tation ullamcorper suscipit lobortis nisl ut aliquip ex ea commodo consequat. Duis autem vel eum iriure dolor in hendrerit in vulputate velit esse molestie consequat.</p>
			</div>
		</div>

		<div class="spacer spacer-medium"></div>

		<h2 class="align-center"><strong>Who we are</strong></h2>
		<h5 class="alttitle align-center">Fueled by Passion</h5>

		<div class="column-section clearfix">
			<div class="column one-third team-member">
				<img src="files/uploads/team1.jpg" alt="SEO NAME"/>
				<div class="team-info offset">
					<h4><strong>Tom Burly</strong></h4>
					<h6 class="alttitle">Creative Director</h6>
					<ul class="socialmedia-widget">
						<li class="facebook"><a href="#"></a></li>
						<li class="twitter"><a href="#"></a></li>
						<li class="dribbble"><a href="#"></a></li>
						<li class="googleplus"><a href="#"></a></li>
					</ul>
				</div>
			</div>
			<div class="column one-third team-member">
				<img src="files/uploads/team2.jpg" alt="SEO NAME"/>
				<div class="team-info offset">
					<h4><strong>Jhon White</strong></h4>
					<h6 class="alttitle">Designer</h6>
					<ul class="socialmedia-widget">
						<li class="twitter"><a href="#"></a></li>
						<li class="behance"><a href="#"></a></li>
						<li class="linkedin"><a href="#"></a></li>
					</ul>
				</div>
			</div>
			<div class="column one-third last-col team-member">
				<img src="files/uploads/team3.jpg" alt="SEO NAME"/>
				<div class="team-info offset">
					<h4><strong>Alice Boga</strong></h4>
					<h6 class="alttitle">Web developer</h6>
					<ul class="socialmedia-widget">
						<li class="facebook"><a href="#"></a></li>
						<li class="twitter"><a href="#"></a></li>
						<li class="dribbble"><a href="#"></a></li>
						<li class="mail"><a href="#"></a></li>
					</ul>
				</div>
			</div>
		</div>

	</div> <!-- END .wrapper -->

	<div class="spacer spacer-big"></div>

	<div class="horizontal-section parallax-section" data-parallax-image="files/uploads/about-parallax.jpg" style="height: 500px;">

		<!-- READ -> style:height is set because we don't have any content -->

	</div> <!-- END .horizontal-section -->

	<div class="spacer spacer-big"></div>

	<div class="wrapper">

		<h2 class="align-center"><strong>How we work</strong></h2>
		<h5 class="alttitle align-center">One of our principles is constant internal communication.</h5>

		<div class="column-section clearfix">
			<div class="column one-third align-center">
				<i class="ion ion-ios-pulse ion-4x colored"></i>
				<h5>Keep pulse going</h5>
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
			</div>
			<div class="column one-third align-center">
				<i class="ion ion-ios-infinite-outline ion-4x colored"></i>
				<h5>Pass the limits</h5>
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
			</div>
			<div class="column one-third last-col align-center">
				<i class="ion ion-ios-lightbulb-outline ion-4x colored"></i>
				<h5>Great Ideas</h5>
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
			</div>
		</div>

		<div class="spacer spacer-small"></div>

		<h5 class="alttitle align-center">Get in touch with us</h5>
		<p class="align-center"><a class="sr-button sr-button2" href="contact.html">Contact</a></p>

	</div> <!-- END .wrapper -->

	<div class="spacer spacer-big"></div>

</section>
<!-- PAGEBODY -->
<?php
require_once 'footer.php';
?>
