<?php
require_once 'header.php';
?>
<!-- HERO  -->
<section id="hero" class="hero-full parallax-section text-light" data-parallax-image="files/uploads/hero-service.jpg">
	<div class="page-title">
		<h1><strong>Services</strong></h1>
	</div>
	<a id="scroll-down" href="#">Scroll Down</a>
</section>
<!-- HERO -->
<!-- PAGEBODY -->
<section id="page-body">

	<div class="spacer spacer-small"></div>

	<div class="wrapper">

		<div class="wolf-grid clearfix">

			<div class="wolf-item wfull wright wolf-text" data-speed="1.3">
				<div class="wolf-item-inner" style="width:60%;">
					<div class="wolf-media">
						<img src="files/uploads/services-branding.jpg" alt="SEO NAME">
					</div>

					<div class="wolf-caption" style="width:50%;">
						<h3><strong>Branding</strong></h3>
						<h5 class="alttitle">Expresses your personality</h5>
						<p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
						<p><a href="portfolio-single-1.html" class="sr-button sr-button2 mini-button">See Example</a></p>
					</div>
				</div>
			</div>

			<div class="wolf-item wfull wleft wolf-text" data-speed="1.6">
				<div class="wolf-item-inner" style="width:60%;">
					<div class="wolf-media">
						<img src="files/uploads/services-photography.jpg" alt="SEO NAME">
					</div>

					<div class="wolf-caption" style="width:50%;">
						<h3><strong>Photography</strong></h3>
						<h5 class="alttitle">Worth a thousand words</h5>
						<p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
						<p><a href="portfolio-single-2.html" class="sr-button sr-button2 mini-button">See Example</a></p>
					</div>
				</div>
			</div>

			<div class="wolf-item wfull wright wolf-text" data-speed="0.9">
				<div class="wolf-item-inner" style="max-width:60%;">
					<div class="wolf-media">
						<img src="files/uploads/services-print.jpg" alt="SEO NAME">
					</div>

					<div class="wolf-caption" style="width:50%;">
						<h3><strong>Print</strong></h3>
						<h5 class="alttitle">Top notch quality</h5>
						<p>Consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
						<p><a href="portfolio-single-3.html" class="sr-button sr-button2 mini-button">See Example</a></p>
					</div>
				</div>
			</div>

		</div> <!-- END .wolf-grid -->

		<h2 class="align-center"><strong>And much more</strong></h2>

		<div class="column-section clearfix">
			<div class="column one-third align-center">
				<i class="ion ion-ios-monitor-outline ion-4x colored"></i>
				<h5>Web Design</h5>
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
			</div>
			<div class="column one-third align-center">
				<i class="ion ion-ios-videocam-outline ion-4x colored"></i>
				<h5>Video production</h5>
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
			</div>
			<div class="column one-third last-col align-center">
				<i class="ion ion-ios-wineglass-outline ion-4x colored"></i>
				<h5>Event Managment</h5>
				<p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.</p>
			</div>
		</div>

		<div class="spacer spacer-small"></div>

		<h5 class="alttitle align-center">Get in touch with us</h5>
		<p class="align-center"><a class="sr-button sr-button2" href="#">Contact</a></p>

	</div> <!-- END .wrapper -->

	<div class="spacer spacer-big"></div>

</section>
<!-- PAGEBODY -->
<?php
require_once 'footer.php';
?>
