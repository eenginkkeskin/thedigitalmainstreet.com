<!doctype html>
<!--[if lt IE 7]> <html class="no-js ie6 oldie" lang="en"> <![endif]-->
<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" lang="en-US"> <!--<![endif]-->
<head>

	<!-- DEFAULT META TAGS -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
	<title>Main Street Digital Agency | Web Design and web Development</title>
	<meta name="description" content="Main Street Digital is a full service graphic design studio specializing in web design, web development, branding and print graphic design based in Philadelphia, PA."/>
	<!-- DEFAULT META TAGS -->

	<!-- FONTS -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Maven+Pro:400,700,900" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900" rel="stylesheet" type="text/css">
	<link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,700" rel="stylesheet" type="text/css">
	<!-- FONTS -->

	<!-- CSS -->
	<link rel="stylesheet" id="default-style-css"  href="files/css/style.css" type="text/css" media="all" />
	<link rel="stylesheet" id="wolf-css" href="files/css/wolf.css" type="text/css" media="all" />
	<link rel="stylesheet" id="owlcarousel-css" href="files/css/owl.carousel.css" type="text/css" media="all" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
	<link rel="stylesheet" id="ionic-icons-style-css" href="files/css/ionicons.css" type="text/css" media="all" />
	<link rel="stylesheet" id="mqueries-style-css"  href="files/css/mqueries.css" type="text/css" media="all" />
	<!-- CSS -->

	<!-- FAVICON -->
	<link rel="shortcut icon" href="files/uploads/favicon.png"/>
	<!-- FAVICON -->

	<!-- JQUERY LIBRARY -->
	<script src="files/js/jquery-1.9.1.min.js"></script>
	<!-- JQUERY LIBRARY -->

</head>

<body>
<!-- PAGELOADER -->
<div id="page-loader">
	<div class="page-loader-inner">
		<div class="loader"><strong>Loading</strong></div>
	</div>
</div>
<!-- PAGELOADER -->
<!-- PAGE CONTENT -->
<div id="page-content">
	<!-- HEADER -->
	<header id="header">
		<div class="header-inner clearfix">

			<div id="logo" class="left-float show-light-logo">
				<a id="dark-logo" class="logotype" href="/"><img src="files/uploads/logo-avoc-dark.png" srcset="files/uploads/logo-avoc-dark.png 1x, files/uploads/logo-avoc-dark@2x.png 2x" alt="Logo"></a>
				<a id="light-logo" class="logotype" href="/"><img src="files/uploads/logo-avoc-light.png" srcset="files/uploads/logo-avoc-light.png 1x, files/uploads/logo-avoc-light@2x.png 2x" alt="Logo"></a>
			</div>

			<div id="menu" class="right-float clearfix menu-light">
				<a href="#" class="open-nav"><span class="hamburger"></span><span class="text">Menu</span></a>

				<nav id="main-nav">
					<ul>
						<li class="current-menu-item"><a href="/">Home</a></li>
						<li><a href="about.html">About</a></li>
						<li><a href="services.html">Services</a></li>
						<li><a href="contact.html">Contact</a></li>
					</ul>
				</nav>
			</div>

		</div> <!-- END .header-inner -->
	</header>
	<!-- HEADER -->